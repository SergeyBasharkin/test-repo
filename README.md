Inline `code` has `back-ticks around` it.
```swift
var a = "Hello World"
```

```kotlin
public fun() {
	val a = "Hello"
}
```